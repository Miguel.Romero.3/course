---
theme: seriph
background: https://source.unsplash.com/collection/94734566/1920x1080
class: text-center
highlighter: shiki
lineNumbers: false
info: |
  ## Slidev Starter Template
  Presentation slides for developers.

  Learn more at [Sli.dev](https://sli.dev)
drawings:
  persist: false
transition: slide-left
title: Welcome to Slidev
mdc: true
---

# Docker deployment architecture

---

## Docker compose

Dockers compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application's services. Then, with a single command, you create and start all the services from your configuration.

In compose, we define the architecture of our application in a YAML file. This means that we establish what services we need, how they are connected, and how they are configured.

---

## Docker compose (2)

To start the application, we use the `docker-compose up` command in the directory containing the compose file (`{docker-}?compose.{yml,yaml}`). This command will create the resources defined in the compose file, and start the application.

When a service is modified, you just have to run `docker-compose up -d` to update the application. Docker compose will detect the changes and update the resources accordingly, without recreating the unchanged resources.

---

## Components

Compose contains these main components:

- Services: the containers that compose your app (e.g. web server, database, etc.)
- Networks: the networks that connect your services
- Volumes: the volumes that you can use to persist data
- Secrets: the secrets that you can use to store sensitive data

---

## Compose step-by-step

First, we need to define what is the main service of our application. In our example, it is Wordpress.

```yaml
version: '3.1'

services:
  wordpress:
    image: wordpress
    restart: always
    ports:
      - 8080:80
```

This is equivalent to the following command:

```bash
docker run -d -p 8080:80 --restart always wordpress
```

---

## Compose step-by-step (2)

If you try to access the application at http://localhost:8080, you will see that wordpress successfully starts, but it asks for a database.

So, we need to add a database to our application. We will use MySQL.

```yaml
services:
  wordpress: ...
  db:
    image: mysql:8.0
    restart: always
```

This will create an empty MySQL database. We need to configure Wordpress to use this database. This configuration won't works out of the box, because we haven't defined the database name, the user, and the password.

---

## Compose step-by-step (3)

To configure default database name, user, and password for MySQL, we can use environment variables.

```yaml
services:
  wordpress: ...
  db:
    ...
    environment:
      MYSQL_DATABASE: exampledb
      MYSQL_USER: exampleuser
      MYSQL_PASSWORD: examplepass
      MYSQL_RANDOM_ROOT_PASSWORD: '1'
```

Now, if we start the application, Wordpress will ask for the database name, user, and password.

Note that the two container are connected to the same network because Docker compose create a `default` network for the application. Services can communicate with each other using their name as hostname.

---

## Compose step-by-step (4)

As we don't want to enter the database name, user, and password each time we start the application, we can define them in the Wordpress configuration.

```yaml
services:
  wordpress:
    ...
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: exampleuser
      WORDPRESS_DB_PASSWORD: examplepass
      WORDPRESS_DB_NAME: exampledb
  db: ...
```

---

## Compose step-by-step (5)

Finally, we need to persist the data of the database and Wordpress. To do so, we can use volumes.

```yaml
services:
  wordpress:
    ...
    volumes:
      - wordpress:/var/www/html
  db:
    ...
    volumes:
      - db:/var/lib/mysql

volumes:
  wordpress:
  db:
```

---

### Final example

```yaml
version: '3.1'

services:

  wordpress:
    image: wordpress
    restart: always
    ports:
      - 8080:80
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: exampleuser
      WORDPRESS_DB_PASSWORD: examplepass
      WORDPRESS_DB_NAME: exampledb
    volumes:
      - wordpress:/var/www/html

  db:
    image: mysql:8.0
    restart: always
    environment:
      MYSQL_DATABASE: exampledb
      MYSQL_USER: exampleuser
      MYSQL_PASSWORD: examplepass
      MYSQL_RANDOM_ROOT_PASSWORD: '1'
    volumes:
      - db:/var/lib/mysql

volumes:
  wordpress:
  db:
```

---

### Equivalent Docker vanilla command

#### Create the volumes

```bash
docker volume create wordpress
docker volume create db
```

#### Create the Wordpress container

```bash
docker run -d -p 8080:80 --restart always \
  -e WORDPRESS_DB_HOST=db \
  -e WORDPRESS_DB_USER=exampleuser \
  -e WORDPRESS_DB_PASSWORD=examplepass \
  -e WORDPRESS_DB_NAME=exampledb \
  -v wordpress:/var/www/html \
  wordpress
```

---

#### Create the MySQL container

```bash
docker run -d --restart always \
  -e MYSQL_DATABASE=exampledb \
  -e MYSQL_USER=exampleuser \
  -e MYSQL_PASSWORD=examplepass \
  -e MYSQL_RANDOM_ROOT_PASSWORD=1 \
  -v db:/var/lib/mysql \
  mysql:8.0
```

---

By default, the resources created by Docker compose are prefixed with the name of the directory containing the compose file. To change this behavior, you can use the `-p` option.

```bash
docker-compose -p myproject up -d
```

For example, if you define a volume named `wordpress`, it will be named `myproject_wordpress` inside Docker.

---

### Services

Services defines how containers should be created and run. In the example above, we have two services: `wordpress` and `db`.

A service can be built from a Dockerfile, or from an image. In the example above, we use the `wordpress` image, and the `mysql:8.0` image.

Inside the service definition, we can define the following properties:

- `image`: the image to use
- `build`: the Dockerfile to use to build the image (instead of `image`)
- `ports`: the ports to expose
- `environment`: the environment variables to set
- `volumes`: the volumes to mount
- `networks`: the networks to connect to

See the [Services Compose file reference](https://docs.docker.com/compose/compose-file/compose-file-v3/#service-configuration-reference) for more details.

---

### Networks

As said before, Docker compose creates a network named `default` for the application. All services are connected to this network.

What if we want to create different networks for different services? For example, we want to create a network for connecting Wordpress and MySQL, and another network for connecting Wordpress and an external service.

---

```yaml
services:
  wordpress:
    ...
    networks:
      - wordpress # connect to the wordpress network, used by MySQL
      - ext_net # connect to the ext_net network, used by an external service
  db:
    ...
    networks:
      - wordpress # connect to the wordpress network, used by Wordpress

networks:
  wordpress:
  ext_net: # define the ext_net network
    external: true # use an external network
    name: ext_net # the name of the external network in Docker
```

The `ext_net` network is an external network. It means that it is not created by Docker compose, but it is used by the application. In this case, the network must be created before starting the application.

---

# Reverse proxy

---

## What is a reverse proxy?

A reverse proxy is a server that sits in front of web servers and forwards client (e.g. web browser) requests to those web servers. Reverse proxies are typically implemented to help increase security, performance, and reliability.

Let's take an example. You have two web servers, one for Wordpress, and one for Nextcloud. You want to access both applications from the same domain name, but on different paths. For example, you want to access Wordpress at `https://example.com/wordpress`, and Nextcloud at `https://example.com/nextcloud`. You also want to enable HTTPS for both applications.

---

## HTTPS and TLS

In order to understand the role of a reverse proxy, we need to understand how HTTPS works. HTTPS is a protocol that encrypts the communication between the client (e.g. web browser) and the server (e.g. web server). It uses TLS (Transport Layer Security) to encrypt the communication.

To enable HTTPS, the server needs a certificate. A certificate is a file that contains the public key of the server. The certificate is signed by a certificate authority (CA). The CA is a trusted third party that guarantees that the certificate is valid. The client can verify the validity of the certificate by checking the signature of the CA.

Each machine has a list of trusted CAs. When the client receives the certificate, it checks the signature of the CA. If the signature is valid, the client knows that the certificate is valid, and it can use the public key to encrypt the communication.

E.g. Let's Encrypt is a CA that provides free certificates.

---

## Without reverse proxy

![Withouth reverse proxy](/images/without_reverse_proxy.svg)

In this case, Bob wants to access Wordpress and Nextcloud. He sends a request directly to the web server hosting Wordpress, at port 8080, and to the web server hosting Nextcloud, at port 8081.

As you can see, Bob has to remember the port of each application, and he has to type it in the URL. Moreover, the web servers are exposed to the Internet, which is not a good practice. Finally, the web servers may not support HTTPS, so Bob has to use HTTP.

In such a situation, it can be difficult to manage the certificates, and to configure the web servers to use HTTPS, because each web server has to be configured separately.

To solve these problems, we can use a reverse proxy.

---

## With reverse proxy

![With reverse proxy](/images/with_reverse_proxy.svg)

In this case, Bob sends a request to the reverse proxy, at port 443 (HTTPS). The reverse proxy forwards the request to the web servers, at port 8080 and 8081. The reverse proxy also manages the certificates, and enables HTTPS for both applications.

If needed, the reverse proxy can also add authentication, and restrict access to the applications.

The different services can communicate with HTTPS using a self-signed certificate, and the reverse proxy can use a certificate signed by a certificate authority.

---

## Practical example using nginx-proxy

---

[nginx-proxy](https://github.com/nginx-proxy/nginx-proxy) is a reverse proxy that automatically configures itself to work with Docker. It uses the Docker API to listen for events, and dynamically reconfigures itself when containers are started and stopped.

To learn more about nginx-proxy, you can read this blog post from Jason Wilder: [Automated Nginx Reverse Proxy for Docker](http://jasonwilder.com/blog/2014/03/25/automated-nginx-reverse-proxy-for-docker/)

---

### Take the previous example

```yaml
version: '3.1'

services:

  wordpress:
    image: wordpress
    restart: always
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: exampleuser
      WORDPRESS_DB_PASSWORD: examplepass
      WORDPRESS_DB_NAME: exampledb
      VIRTUAL_HOST: example.com # define on which domain name the application is available
      VIRTUAL_PATH: /wordpress # OPTIONAL: define on which path the application is available
    volumes:
      - wordpress:/var/www/html
    networks:
      - wordpress
      - proxy # need to connect to the proxy network since the service is not exposed anymore

  db:
    image: mysql:8.0
    restart: always
    environment:
      MYSQL_DATABASE: exampledb
      MYSQL_USER: exampleuser
      MYSQL_PASSWORD: examplepass
      MYSQL_RANDOM_ROOT_PASSWORD: '1'
    volumes:
      - db:/var/lib/mysql
    networks:
      - wordpress
```

---

```yaml
  nginx-proxy:
    image: nginxproxy/nginx-proxy
    restart: always
    ports:
      - 80:80
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock:ro # mount the Docker socket
    networks:
      - proxy

volumes:
  wordpress:
  db:

networks:
  wordpress:
  proxy:
```

---

As you can see, we have added the `VIRTUAL_HOST` and `VIRTUAL_PATH` environment variables to the Wordpress service. These variables are used by nginx-proxy to configure itself.

Furthermore, we have added the `proxy` network to the Wordpress service. This is because the Wordpress service is not exposed anymore, and we need to connect it to the `proxy` network to allow nginx-proxy to communicate with it.

The `nginx-proxy` service is exposed on port 80 (HTTP), and it is connected to the `proxy` network. We also mount the Docker socket to allow nginx-proxy to listen for events.

Read the [nginx-proxy documentation](https://github.com/nginx-proxy/nginx-proxy/tree/main/docs) for more configuration options.

---

### Adding HTTPS with Let's Encrypt

To add HTTPS, we can use [acme-companion](https://github.com/nginx-proxy/acme-companion). This service runs in parallel with nginx-proxy, and it automatically generates and renews certificates using Let's Encrypt.

Let's Encrypt is a certificate authority (CA) that provides free certificates. ACME companion generates a certificate for a given domain name and sends a request to the CA to sign it. It uses the ACME protocol to verify that the domain name is valid and that the requester is the owner of the domain name.

Eventually, the CA signs the certificate and sends it back to ACME companion.

---

```yaml
services:
  ...
  wordpress:
    ...
    environment:
      ...
      LETSENCRYPT_HOST: example.com # define on which domain name the application is available
      LETSENCRYPT_EMAIL: # OPTIONAL: define the email address used by Let's Encrypt

  nginx-proxy:
    ...
    ports:
      - 80:80
      - 443:443 # expose port 443 for HTTPS
    volumes:
      - certs:/etc/nginx/certs
      - vhost:/etc/nginx/vhost.d
      - html:/usr/share/nginx/html
      - /var/run/docker.sock:/tmp/docker.sock:ro

  acme-companion:
    image: nginxproxy/acme-companion
    restart: always
    volumes:
      - certs:/etc/nginx/certs:rw
      - vhost:/etc/nginx/vhost.d
      - html:/usr/share/nginx/html
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - acme:/etc/acme.sh
    environment:
      DEFAULT_EMAIL: # define the email address used by Let's Encrypt

volumes:
  ...
  certs: # contains the certificates generated by acme-companion
  vhost: # contains the configuration files for each application
  html:
  acme:
```

---

To use Let's Encrypt, each application must have a valid domain name. Let's Encrypt will check that the domain name is valid before generating the certificate. To achieve this, the acme-companion service will start a temporary web server to serve a challenge file. Let's Encrypt will then check that the challenge file is available at the domain name.

By "valid domain name", one means a domain name that is registered and that points to the IP address of the server.

For more configuration options, see the [acme-companion documentation](https://github.com/nginx-proxy/acme-companion/tree/main/docs)

![Reverse proxy with HTTPS](/images/reverse_proxy.svg)
