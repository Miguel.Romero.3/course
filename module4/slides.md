---
theme: seriph
background: https://source.unsplash.com/collection/94734566/1920x1080
class: text-center
highlighter: shikiji
lineNumbers: false
info: |
  ## Slidev Starter Template
  Presentation slides for developers.

  Learn more at [Sli.dev](https://sli.dev)
drawings:
  persist: false
transition: slide-left
title: Welcome to Slidev
mdc: true
---

# Module 4: Kubernetes - Introduction

*Containerization and Orchestration Technologies*

---

## What is Kubernetes?

Kubernetes is an open-source container orchestration platform. It automates the deployment, scaling, and management of containerized applications.

For instance, Kubernetes can:

- Automatically deploy containers across a cluster of physical or virtual machines.
- Scale the number of containers running based on resource utilization.
- Load balance network traffic across a cluster of containers.
- Orchestrate the storage and retrieval of data associated with containers.
- Manage rollouts and rollbacks of containers.
- Provide a platform for implementing CI/CD workflows.
- And much more!

---

## Why Kubernetes? (1)

Let's say you have a web application that you want to deploy to production. You could deploy the application to a single server, but what happens if that server goes down? Your application will go down with it.

You could deploy the application to multiple servers, but:

- How do you load balance traffic across those servers?
- How do you ensure that the application is always available?
- How do you ensure that the application is always up-to-date?
- How do you ensure that the application is always running at peak performance?

You could write custom scripts to handle all of these tasks, but that would be a lot of work.

---

## Why Kubernetes? (2)

Docker compose is somehow an **orchestration tool**, but it is limited to a single host.

A Kubernetes **cluster** is a group of servers that run Kubernetes. Each server in the cluster is called a **node**. A node may be a physical machine or a virtual machine. Each node contains a set of resources managed by Kubernetes.

A node can be a **master node** or a **worker node**. A master node is responsible for managing the cluster. A worker node is responsible for running containers.

An interface, called _Ingress_, is used to load balance traffic across the nodes in the cluster. The Ingress interface can be configured to route traffic to different services based on the URL path or hostname.

---

## How Kubernetes Works (1)

Kubernetes uses a **declarative model** to manage the state of the cluster. You declare the desired state of the cluster in a YAML file called a **manifest**. Kubernetes then ensures that the current state of the cluster matches the desired state.

For example, if we have a manifest that specifies that we want to run three instances of a web application, Kubernetes will ensure that three instances of the application are running at all times. If one of the instances goes down, Kubernetes will automatically restart it.

---

## Kubernetes Core Components

Kubernetes is a stack of technologies that work together to provide a platform for running containerized applications:

- Kubernetes API Server
- Container Runtime
- Kubelet
- Kubernetes Controller Manager
- Kubernetes Scheduler
- etcd

---

### Kubernetes API Server

The API server is the central management point for the cluster. It is present on master nodes and exposes an HTTP API that can be used to manage the cluster.

The API can be accessed using the kubectl command-line tool or directly using the HTTP API.

For example, you can use the kubectl command-line tool to list the nodes in the cluster:

```bash
$ kubectl get nodes
NAME           STATUS   ROLES    AGE   VERSION
clusterlab-3   Ready    <none>   75d   v1.28.3
clusterlab-4   Ready    <none>   75d   v1.28.3
clusterlab-1   Ready    <none>   76d   v1.28.3
clusterlab-2   Ready    <none>   75d   v1.28.3
```

This command will send an HTTP request to the API server to list the nodes in the cluster.

---

### Container Runtime

The container runtime is the software that is used to run containers and is compliant with the Kubernetes [CRI (Container Runtime Interface)](https://kubernetes.io/docs/concepts/architecture/cri/). Some examples of container runtimes are *containerd*, *CRI-O*, and *cri-dockerd*.

The container runtime is responsible for starting and stopping containers on the node. It is also responsible for managing the lifecycle of containers, such as restarting containers that have crashed. The container runtime is an abstraction layer that uses the underlying operating system (module 1 memories: namespaces, cgroups, ...) to automate the management of containers.

---

### Kubelet

The kubelet is the agent (a piece of software) that runs on each worker node in the cluster. It is responsible for managing the containers that run on the node.

It rely on the container runtime to manage the containers.

---

### Kubernetes Controller Manager

The controller manager is responsible for managing the controllers that run on the cluster. A controller is a piece of software that is responsible for managing a specific aspect of the cluster. For example, the ReplicaSet controller is responsible for ensuring that the number of replicas of a given pod matches the desired number.

---

### Kubernetes Scheduler

The scheduler is responsible for scheduling pods to run on nodes in the cluster. It takes into account factors such as resource requirements, affinity/anti-affinity rules, and taints/tolerations when making scheduling decisions. The scheduler is responsible for ensuring that pods are scheduled in a way that maximizes resource utilization and minimizes the risk of failure.

---

### etcd

**etcd** is a distributed key-value store that is used to store the state of the cluster. It is used by the Kubernetes API server to store information about the cluster, such as the current state of the cluster and the desired state of the cluster.

---
layout: two-cols-header
---

## Master and Worker Nodes

::left::

### Master Nodes 

- Global cluster management
- API Server
- Scheduling
- Replcation controller
- Etcd
- Redundancy for high availability

::right::

### Worker Nodes

- Workload execution
- Run pods
- Responsible for resource allocation
- Kubelet
- Kube-proxy
- Container runtime

---

## Different distributions of Kubernetes (1)

- [Minikube](https://minikube.sigs.k8s.io/docs/)
    - Can use *containerd*, *CRI-O*, or *docker* as container runtime, default may vary depending on the OS or the version
    - Runs a single-node Kubernetes cluster inside a VM on your laptop
    - Useful for development and testing
- [MicroK8s](https://microk8s.io/)
    - Uses *containerd* as container runtime
    - Runs a node Kubernetes cluster on your laptop, desktop, or VM
    - Easy to install and use
    - Can configure a multi-node cluster

---

## Different distributions of Kubernetes (2)

- [k3s](https://k3s.io/)
  - Uses *containerd* as default container runtime, but can use *docker* or *nvidia container runtime* as well
  - Lightweight Kubernetes distribution
  - Optimized for IoT and edge computing
  - Can run on ARM devices
- [OpenShift](https://www.openshift.com/)
  - Uses *cri-o* as container runtime
    - Master nodes use *cri-o* and worker nodes can use *cri-o* or *docker*
  - Enterprise Kubernetes distribution
  - Provides additional features on top of Kubernetes
  - Can manage multiple Kubernetes clusters

---

## Example of Cluster

- Container
- pod
- deployment
- service
- ingress
- namespace
- net
